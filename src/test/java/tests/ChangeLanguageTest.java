package tests;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import pageobjects.BasePage;
import pageobjects.ChangeLanguagePage;
import pageobjects.LoginPage;

public class ChangeLanguageTest extends BaseTest {

    @Test
    void shouldCorrectChangeCountryToGerman() throws InterruptedException {
        BasePage basePage = new BasePage(driver, wait);
        basePage.open();
        ChangeLanguagePage changeLanguagePage = new ChangeLanguagePage(driver, wait);
        changeLanguagePage.changeCountryToGermany();
        Assertions.assertTrue(changeLanguagePage.isCorrectChangeCountryToGermany());
    }
    @Test
    void shouldCorrectChangeCountryToRussia() throws InterruptedException {
        BasePage basePage = new BasePage(driver, wait);
        basePage.open();
        ChangeLanguagePage changeLanguagePage = new ChangeLanguagePage(driver, wait);
        changeLanguagePage.changeCountryToRussia();
        Assertions.assertTrue(changeLanguagePage.isCorrectChangeCountryToRussia());
    }
    @Test
    void shouldCorrectChangeCountryToVietnamese() throws InterruptedException {
        BasePage basePage = new BasePage(driver, wait);
        basePage.open();
        ChangeLanguagePage changeLanguagePage = new ChangeLanguagePage(driver, wait);
        changeLanguagePage.changeCountryToVietnamese();
        Assertions.assertTrue(changeLanguagePage.isCorrectChangeCountryToVietnamese());
    }
}
