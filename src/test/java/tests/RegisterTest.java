package tests;

import com.github.javafaker.Faker;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import pageobjects.LoginPage;
import pageobjects.RegisterPage;

public class RegisterTest extends BaseTest{
    @Test
    void register() throws InterruptedException {
        RegisterPage registerPage = new RegisterPage(driver, wait);
        registerPage.open();
        Faker faker = new Faker();
        registerPage.register (faker.name().firstName() + "@dispotable.com", "Qwas1234",
                "Roman", "Test", "789789789");
        Assertions.assertTrue(registerPage.isSuccessRegister());
    }
}
