package tests;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import pageobjects.LoginPage;


public class LoginTest extends tests.BaseTest {

    @Test
    void shouldDispalyInvalidEmailAlertWhenWrongEmailIsProvided() throws InterruptedException {
        LoginPage loginPage = new LoginPage(driver, wait);
        loginPage.open();
        loginPage.login("example@dispotable.com", "Test123!");
        Assertions.assertTrue(loginPage.isAuthenticationErrorDisplay());
    }

    @Test
    void shouldDisplayErrorWhenNoEmailProvided() throws InterruptedException {
        LoginPage loginPage = new LoginPage(driver, wait);
        loginPage.open();
        loginPage.login("", "WrongPassword");
        Assertions.assertTrue(loginPage.isAuthenticationErrorDisplay());
    }

    @Test
    void shouldDisplayErrorWhenNoPasswordProvided() throws InterruptedException {
        LoginPage loginPage = new LoginPage(driver, wait);
        loginPage.open();
        loginPage.login("testautomation@dispotable.com", "");
        Assertions.assertTrue(loginPage.isAuthenticationErrorDisplay());
    }

    @Test
    void shouldCorrectLogin() throws InterruptedException {
        LoginPage loginPage = new LoginPage(driver, wait);
        loginPage.open();
        loginPage.login("user@phptravels.com", "demouser");
        Assertions.assertTrue(loginPage.shouldSuccessLogin());
    }
}