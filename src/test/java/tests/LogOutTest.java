package tests;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import pageobjects.BasePage;
import pageobjects.LogOutPage;
import pageobjects.LoginPage;

public class LogOutTest extends BaseTest {
    @Test
    void shouldCorrectLogOut() throws InterruptedException {
        LoginPage loginPage = new LoginPage(driver, wait);
        loginPage.open();
        loginPage.login("user@phptravels.com", "demouser");
        LogOutPage logOutPage = new LogOutPage(driver,wait);
        logOutPage.logOut();
        Assertions.assertTrue(logOutPage.isCorrectLogOut());
    }
}
