package tests;

import org.junit.jupiter.api.Test;
import pageobjects.BasePage;
import pageobjects.CurrencyPage;

public class CurrencyTest extends BaseTest {
    @Test
    void shouldCorrectChangeCurrencyToJPY() throws InterruptedException {
        BasePage basePage = new BasePage(driver, wait);
        basePage.open();
        CurrencyPage currencyPage = new CurrencyPage(driver, wait);
        currencyPage.changeCurrencyToJPY();
        currencyPage.isCorrectChangeCurrencyToJPY();
    }

    @Test
    void shouldCorrectChangeCurrencyToCNY() throws InterruptedException {
        BasePage basePage = new BasePage(driver, wait);
        basePage.open();
        CurrencyPage currencyPage = new CurrencyPage(driver, wait);
        currencyPage.changeCurrencyToCNY();
        currencyPage.isCorrectChangeCurrencyToCNY();
    }

    @Test
    void shouldCorrectChangeCurrencyToRUB() throws InterruptedException {
        BasePage basePage = new BasePage(driver, wait);
        basePage.open();
        CurrencyPage currencyPage = new CurrencyPage(driver, wait);
        currencyPage.changeCurrencyToRUB();
        currencyPage.isCorrectChangeCurrencyToRUB();
    }

    @Test
    void shouldCorrectChangeCurrencyToKWD() throws InterruptedException {
        BasePage basePage = new BasePage(driver, wait);
        basePage.open();
        CurrencyPage currencyPage = new CurrencyPage(driver, wait);
        currencyPage.changeCurrencyToKWD();
        currencyPage.isCorrectChangeCurrencyToKWD();
    }
}