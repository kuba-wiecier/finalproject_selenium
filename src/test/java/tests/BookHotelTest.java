package tests;

import org.junit.jupiter.api.Test;
import pageobjects.BasePage;
import pageobjects.BookHotelPage;
import pageobjects.SearchHotelsPage;

public class BookHotelTest extends BaseTest {
    @Test
    void shouldCorrectBookedHotels() throws InterruptedException {
        BasePage basePage = new BasePage(driver, wait);
        basePage.open();
        BookHotelPage bookHotelPage = new BookHotelPage(driver, wait);
        bookHotelPage.goToBooking();
        bookHotelPage.selectRoom();
        bookHotelPage.signInAsUserWithAccount("user@phptravels.com", "demouser");

    }
}
