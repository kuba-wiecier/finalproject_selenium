package tests;

import org.junit.jupiter.api.Test;
import pageobjects.BasePage;
import pageobjects.SearchHotelsPage;

public class SearchHotelsTest extends BaseTest {
    @Test
    void shouldCorrectSearchHotels() throws InterruptedException {
        BasePage basePage = new BasePage(driver, wait);
        basePage.open();
        SearchHotelsPage searchHotelsPage = new SearchHotelsPage(driver,wait);
        searchHotelsPage.inputDestination();
        searchHotelsPage.inputCheckIn();
        searchHotelsPage.inputCheckOut();
        searchHotelsPage.clickSearchForHotels();
        searchHotelsPage.isCorrectSearchForHotels();

    }
}