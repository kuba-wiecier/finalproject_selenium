package pageobjects;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class LogOutPage extends BasePage {

    public LogOutPage(WebDriver driver, WebDriverWait wait) {
        super(driver, wait);
    }
    @FindBy(xpath = "//*[@class='dropdown-item tr']")
    WebElement logOutLink;

    @FindBy(xpath = "//*[@class='d-none d-md-block fl']")
    WebElement userDropdown;

    @FindBy(xpath = "//*[@class='dropdown dropdown-login dropdown-tab']//*[@id='dropdownCurrency']")
    WebElement myAccountDropdown;


    public void logOut(){
        //wait.until(ExpectedConditions.visibilityOf(userDropdown));
        userDropdown.click();
        //wait.until(ExpectedConditions.visibilityOf(logOutLink));
        logOutLink.click();
    }
    public boolean isCorrectLogOut(){
        wait.until(ExpectedConditions.visibilityOf(myAccountDropdown));
        return myAccountDropdown.getText().contains("MY ACCOUNT");
    }
}