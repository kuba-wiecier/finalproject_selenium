package pageobjects;

import org.junit.jupiter.api.Test;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

public class RegisterPage extends BasePage {
    final String REGISTER_URL = MAIN_URL + "/register";

    @FindBy(xpath = "//*[@name='firstname']")
    WebElement firstName;

    @FindBy(xpath = "//*[@name='lastname']")
    WebElement lastName;

    @FindBy(xpath = "//*[@name='phone']")
    WebElement phoneNumber;

    @FindBy(xpath = "//*[@name='email']")
    WebElement emailField;

    @FindBy(xpath = "//*[@name='password']")
    WebElement passwordField;

    @FindBy(xpath = "//*[@name='confirmpassword']")
    WebElement passwordConfirm;

    @FindBy(xpath = "//*[@class='dropdown dropdown-login dropdown-tab']")
    WebElement loginDropdown;

    public RegisterPage(WebDriver driver, WebDriverWait wait) {
        super(driver, wait);
    }

    public void open() {
        driver.get(REGISTER_URL);
    }

    public void register(String email, String password, String name, String nameLast, String mobilePhone)
            throws InterruptedException {

        firstName.sendKeys(name);
        lastName.sendKeys(nameLast);
        phoneNumber.sendKeys(mobilePhone);
        emailField.sendKeys(email);
        passwordField.sendKeys(password);
        passwordConfirm.sendKeys(password);
        passwordConfirm.sendKeys(Keys.ENTER);
    }

    public boolean isSuccessRegister() {
        wait.until(ExpectedConditions.textToBePresentInElement(loginDropdown, "ROMAN"));
        return loginDropdown.getText().contains("ROMAN");

    }
}