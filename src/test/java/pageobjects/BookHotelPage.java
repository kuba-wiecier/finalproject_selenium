package pageobjects;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import tests.SearchHotelsTest;

public class BookHotelPage extends BasePage {
    public BookHotelPage(WebDriver driver, WebDriverWait wait) {
        super(driver, wait);
    }

    //@FindBy(xpath = "//*[@class='custom-control custom-checkbox']//*[@for='32']") nie potrafiłem stworzyć selectora
    // który będzie widoczny w selenium i klikal w dany element, dlatego skopiowany bardzo brzydki ale działajacy.
    @FindBy (xpath = "//*[@id='detail-content-sticky-nav-02']/form/div/div[2]/div/div[2]/div/div[2]/h5/div/label")
    WebElement firstSelectRoomCheckbox;

    @FindBy(xpath = "//*[@id='signintab']")
    WebElement signInOption;

    @FindBy(xpath = "//*[@class='book_button btn btn-success btn-block btn-lg chk']")
    WebElement bookNowBtn;

    @FindBy(xpath = "//*[@id='loginform']//*[@name='username']")
    WebElement emailFieldBooking;

    @FindBy(xpath = "//*[@id='loginform']//*[@name='password']")
    WebElement passwordFieldBooking;

    @FindBy(xpath = "//*[@class='col-12 col-lg-8']//*[@class='heading-title']")
    WebElement bookingDetailsHeader;

    public void goToBooking() throws InterruptedException {
        BasePage basePage = new BasePage(driver, wait);
        basePage.open();
        SearchHotelsPage searchHotelsPage = new SearchHotelsPage(driver, wait);
        searchHotelsPage.inputDestination();
        searchHotelsPage.inputCheckIn();
        searchHotelsPage.inputCheckOut();
        searchHotelsPage.clickSearchForHotels();
    }

    public void selectRoom() throws InterruptedException {
        JavascriptExecutor jse = (JavascriptExecutor)driver;
        jse.executeScript("window.scrollBy(0,1800)");
        wait.until(ExpectedConditions.visibilityOf(firstSelectRoomCheckbox));
        wait.until(ExpectedConditions.elementToBeClickable(firstSelectRoomCheckbox));
        firstSelectRoomCheckbox.click();
        Thread.sleep(4000);
        bookNowBtn.click();
    }

    public void signInAsUserWithAccount(String emailValue, String passwordValue) {
        wait.until(ExpectedConditions.elementToBeClickable(signInOption));
        signInOption.click();
        emailFieldBooking.sendKeys(emailValue);
        passwordFieldBooking.sendKeys(passwordValue);
        passwordFieldBooking.sendKeys(Keys.ENTER);
    }

    public boolean shouldSuccessBooked() {
        wait.until(ExpectedConditions.visibilityOf(bookingDetailsHeader));
        return bookingDetailsHeader.getText().contains("Booking Details");
    }
}
