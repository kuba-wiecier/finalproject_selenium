package pageobjects;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import tests.RegisterTest;

public class LoginPage extends BasePage {

    final String LOGIN_URL = MAIN_URL + "/login";

    @FindBy(name = "username")
    WebElement email;

    @FindBy(name = "password")
    WebElement password;

    @FindBy(xpath = "//*[@class='alert alert-danger']")
    WebElement authenticationAlert;

    public LoginPage(WebDriver driver, WebDriverWait wait) {
        super(driver, wait);
    }

    public void login(String emailValue, String passwordValue) {
        wait.until(ExpectedConditions.visibilityOf(email));
        email.sendKeys(emailValue);
        password.sendKeys(passwordValue);
        password.sendKeys(Keys.ENTER);
    }

    public void open() {
        driver.get(LOGIN_URL);
    }

    public boolean isAuthenticationErrorDisplay() {
        wait.until(ExpectedConditions.visibilityOf(authenticationAlert));
        return authenticationAlert.getText().contains("Invalid Email or Password");
    }

    public boolean shouldSuccessLogin() {
        wait.until(ExpectedConditions.textToBePresentInElement(loginDropdown, "DEMO"));
        return loginDropdown.getText().contains("DEMO");
    }
}