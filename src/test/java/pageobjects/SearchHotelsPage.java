package pageobjects;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class SearchHotelsPage extends BasePage {
    public SearchHotelsPage(WebDriver driver, WebDriverWait wait) {
        super(driver, wait);
    }

    @FindBy(xpath = "//*[@id='checkin']")
    WebElement checkIn;

    @FindBy(xpath = "//*[@id='checkout']")
    WebElement checkOut;

    @FindBy(xpath = "//*[@class='select2-container form-control hotelsearch locationlistHotels']")
    WebElement destinationSelect;

    @FindBy(xpath = "//*[@id='select2-drop']/div/input")
    WebElement ds1;

    @FindBy(xpath = "//*[@class='select2-match']")
    WebElement searchPhrase;

    @FindBy(xpath = "//*[@class='col-lg-2 col-sm-12 col-xs-12']//*[@class='btn btn-primary btn-block']")
    WebElement searchBtn;

    @FindBy(xpath = "//*[@id='detail-content-sticky-nav-00']")
    WebElement hotelNameHeader;

    public void inputDestination() throws InterruptedException {
        wait.until(ExpectedConditions.visibilityOf(destinationSelect));
        destinationSelect.click();
        wait.until(ExpectedConditions.visibilityOf(ds1));
        ds1.sendKeys("Grand Plaza Service");
        searchPhrase.click();
    }

    public void inputCheckIn() {
        wait.until(ExpectedConditions.invisibilityOfElementLocated(By.className("form-control form-readonly-control")));
        checkIn.click();
        checkIn.clear();
        checkIn.sendKeys("20/09/2021");
    }

    public void inputCheckOut() {
        wait.until(ExpectedConditions.invisibilityOfElementLocated(By.className("form-control form-readonly-control")));
        checkOut.click();
        checkOut.clear();
        checkOut.sendKeys("12/10/2021");
    }

    public void clickSearchForHotels() {
        searchBtn.click();
    }

    public boolean isCorrectSearchForHotels(){
        wait.until(ExpectedConditions.visibilityOf(hotelNameHeader));
        return hotelNameHeader.getText().contains("Grand Plaza Apartments");
    }
}
