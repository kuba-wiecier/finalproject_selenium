package pageobjects;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class CurrencyPage extends BasePage {
    public CurrencyPage(WebDriver driver, WebDriverWait wait) {
        super(driver, wait);
    }

    @FindBy(xpath = "//*[@class='dropdown dropdown-currency']")
    WebElement dropdownCurrency;

    @FindBy(xpath = "//*[@data-code='17']")
    WebElement jpyCurrency;

    @FindBy(xpath = "//*[@data-code='19']")
    WebElement cnyCurrency;

    @FindBy(xpath = "//*[@data-code='22']")
    WebElement rubCurrency;

    @FindBy(xpath = "//*[@data-code='22']")
    WebElement kwdCurrency;

    public void changeCurrencyToJPY() {
        wait.until(ExpectedConditions.visibilityOf(dropdownCurrency));
        dropdownCurrency.click();
        wait.until(ExpectedConditions.visibilityOf(jpyCurrency));
        jpyCurrency.click();
    }
    public boolean isCorrectChangeCurrencyToJPY() {
        wait.until(ExpectedConditions.visibilityOf(dropdownCurrency));
        return dropdownCurrency.getText().contains("JPY");
    }
    public void changeCurrencyToCNY() {
        wait.until(ExpectedConditions.visibilityOf(dropdownCurrency));
        dropdownCurrency.click();
        wait.until(ExpectedConditions.visibilityOf(cnyCurrency));
        jpyCurrency.click();
    }
    public boolean isCorrectChangeCurrencyToCNY() {
        wait.until(ExpectedConditions.visibilityOf(dropdownCurrency));
        return dropdownCurrency.getText().contains("CNY");
    }
    public void changeCurrencyToRUB() {
        wait.until(ExpectedConditions.visibilityOf(dropdownCurrency));
        dropdownCurrency.click();
        wait.until(ExpectedConditions.visibilityOf(rubCurrency));
        jpyCurrency.click();
    }
    public boolean isCorrectChangeCurrencyToRUB() {
        wait.until(ExpectedConditions.visibilityOf(dropdownCurrency));
        return dropdownCurrency.getText().contains("RUB");
    }
    public void changeCurrencyToKWD() {
        wait.until(ExpectedConditions.visibilityOf(dropdownCurrency));
        dropdownCurrency.click();
        wait.until(ExpectedConditions.visibilityOf(kwdCurrency));
        jpyCurrency.click();
    }
    public boolean isCorrectChangeCurrencyToKWD() {
        wait.until(ExpectedConditions.visibilityOf(dropdownCurrency));
        return dropdownCurrency.getText().contains("KWD");
    }
}
