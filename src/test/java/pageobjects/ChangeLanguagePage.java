package pageobjects;

import org.junit.jupiter.api.Assertions;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class ChangeLanguagePage extends BasePage {
    public ChangeLanguagePage(WebDriver driver, WebDriverWait wait) {
        super(driver, wait);
    }

    @FindBy(xpath = "//*[@id='dropdownLangauge']")
    WebElement dropdownLangauge;

    @FindBy(xpath = "//*[@id='de']")
    WebElement German;

    @FindBy(xpath = "//*[@id='ru']")
    WebElement Russian;

    @FindBy(xpath = "//*[@id='vi']")
    WebElement Vietnamese;

    public void changeCountryToGermany() {
        wait.until(ExpectedConditions.visibilityOf(dropdownLangauge));
        dropdownLangauge.click();
        wait.until(ExpectedConditions.visibilityOf(German));
        German.click();
    }

    public void changeCountryToRussia() {
        wait.until(ExpectedConditions.visibilityOf(dropdownLangauge));
        dropdownLangauge.click();
        wait.until(ExpectedConditions.visibilityOf(Russian));
        Russian.click();
    }

    public void changeCountryToVietnamese() {
        wait.until(ExpectedConditions.visibilityOf(dropdownLangauge));
        dropdownLangauge.click();
        wait.until(ExpectedConditions.visibilityOf(Vietnamese));
        Vietnamese.click();
    }

    public boolean isCorrectChangeCountryToGermany() {
        String URL = driver.getCurrentUrl();
        Assertions.assertEquals(URL, "https://www.phptravels.net/de");
        return URL.equals("https://www.phptravels.net/de");
    }

    public boolean isCorrectChangeCountryToRussia() {
        String URL = driver.getCurrentUrl();
        Assertions.assertEquals(URL, "https://www.phptravels.net/ru");
        return URL.equals("https://www.phptravels.net/ru");
    }

    public boolean isCorrectChangeCountryToVietnamese() {
        String URL = driver.getCurrentUrl();
        Assertions.assertEquals(URL, "https://www.phptravels.net/vi");
        return URL.equals("https://www.phptravels.net/vi");
    }
}