package pageobjects;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class SearchFlightsPage extends BasePage {
    public SearchFlightsPage(WebDriver driver, WebDriverWait wait) {
        super(driver, wait);
    }

    @FindBy(xpath = "//*[@id='search']/div/div/div/div/div/nav/ul/li[2]/a")
    WebElement flightsSearch;

    @FindBy(xpath = "//*[@id='s2id_location_from']/a")
    WebElement searchFrom;

    @FindBy(xpath = "//*[@id='s2id_location_to']/a/span[1]")
    WebElement searchTo;

    @FindBy(xpath = "//*[@id='select2-drop']/div/input")
    WebElement searchTo2;

    //@FindBy(xpath = "//*[@id='select2-drop']")
    @FindBy(xpath = "//*[@id='select2-drop']/ul/li/div/span")
    WebElement searchDrop;

    @FindBy(xpath = "//*[@class='select2-result-label']")
    WebElement searchDropTo;

    @FindBy(xpath = "//*[@id='FlightsDateStart']")
    WebElement depart;


    public void inputAirportsDetails() throws InterruptedException {
        wait.until(ExpectedConditions.elementToBeClickable(flightsSearch));
        flightsSearch.click();
        wait.until(ExpectedConditions.visibilityOf(searchFrom));
        searchFrom.click();
        wait.until(ExpectedConditions.visibilityOf(searchTo));
        searchFrom.sendKeys("Kital");
        wait.until(ExpectedConditions.visibilityOf(searchDrop));
        searchDrop.click();
        wait.until(ExpectedConditions.elementToBeClickable(searchTo));
        searchTo.click();
        Thread.sleep(5000);
        searchTo2.sendKeys("Wid");
        searchDropTo.click();

    }

    public void inputDepart() {
        wait.until(ExpectedConditions.visibilityOf(depart));
        depart.click();
        //depart.clear();
        //depart.sendKeys("");
    }
}